using System;
using System.IO;
using foodFriend.Droid;
using Xamarin.Forms;
[assembly: Dependency(typeof(foodFriend.Droid.SQLite_Android))]

namespace foodFriend.Droid
{
    public class SQLite_Android : ISQLite
    {
        public SQLite_Android()
        {
        }

        #region ISQLite implementation
        public SQLite.SQLiteConnection GetConnection()
        {
            var fileName = "foodfriend.db3";
            var documentsPath = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            var path = Path.Combine(documentsPath, fileName);

            //var platform = new SQLite.Net.Platform.XamarinAndroid.SQLitePlatformAndroid();
            var connection = new SQLite.SQLiteConnection(path);

            return connection;
        }

        #endregion
    }
}

