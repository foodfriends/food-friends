﻿using System;
using System.Collections.Generic;
using System.Linq;
using Rg.Plugins.Popup.IOS;

using Foundation;
using UIKit;

namespace foodFriend.iOS
{
	[Register("AppDelegate")]
	public partial class AppDelegate : global::Xamarin.Forms.Platform.iOS.FormsApplicationDelegate
	{
		public override bool FinishedLaunching(UIApplication app, NSDictionary options)
		{
            Popup.Init();
            global::Xamarin.Forms.Forms.Init();
            global::Xamarin.FormsMaps.Init();
            SQLitePCL.CurrentPlatform.Init();
            Microsoft.WindowsAzure.MobileServices.CurrentPlatform.Init();
            LoadApplication(new App());

			return base.FinishedLaunching(app, options);
		}
	}
}

