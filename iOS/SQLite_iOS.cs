using System;
using System.IO;
using foodFriend.iOS;
using SQLite;
using Xamarin.Forms;
[assembly: Dependency(typeof(foodFriend.iOS.SQLite_iOS))]
namespace foodFriend.iOS
{
    public class SQLite_iOS : ISQLite
    {
        public SQLite_iOS()
        {
        }
        #region ISQLite implementation

        public SQLite.SQLiteConnection GetConnection()
        {
            var fileName = "foodfriend.db3";
            var documentsPath = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            var libraryPath = Path.Combine(documentsPath, "..", "Library");
            var path = Path.Combine(libraryPath, fileName);


            var connection = new SQLite.SQLiteConnection(path);

            return connection;
        }

        #endregion
    }
}

