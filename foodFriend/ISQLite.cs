﻿/*
 * Project Name: food Friends
 */
using System;
using SQLite;
using Xamarin.Forms;

[assembly: Dependency(typeof(foodFriend.ISQLite))]

namespace foodFriend
{
    public interface ISQLite
    {
        SQLiteConnection GetConnection();
    }
}
