﻿/*
 * Project Name: food Friends
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Rg.Plugins.Popup.Extensions;
using Rg.Plugins.Popup.Pages;
using Xamarin.Forms;

namespace foodFriend
{
    public partial class PopUpImage : PopupPage
    {
       
        public PopUpImage(string image_name)
        {
            InitializeComponent();

            image_fullsize.Source = image_name;

            var tapGestureRecognizer = new TapGestureRecognizer();
            tapGestureRecognizer.Tapped += async (sender, e) =>
            {
                image_fullsize.Opacity = .5;
                await Task.Delay(100);
                image_fullsize.Opacity = 1;
                //get back to detail page
                await Navigation.PopPopupAsync();
            };
            image_fullsize.GestureRecognizers.Add(tapGestureRecognizer);
        }
    }
}
