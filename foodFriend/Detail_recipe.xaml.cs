﻿/*
 * Project Name: food Friends
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace foodFriend
{
    public partial class Detail_recipe : ContentPage
    {

        private RecipeDB db_recipe;
        private Recipe recipe;

        public Detail_recipe(string namereci)
        {
            InitializeComponent();
            Title = "Details";
            db_recipe = new RecipeDB();

            //find the recipe record by name
            recipe = db_recipe.GetRecipe(namereci);
            getContent();
        }
        //show the infor in page
        public void getContent()
        {

            lbl_name.Text = recipe.Name;
            string[] arrayIngredients = recipe.Ingredients.Split('~');
            foreach (string x in arrayIngredients)
            {
                lbl_ingredients.Text += x + "\n";
            }
            lbl_category.Text = recipe.Category;
            string[] arraySteps = recipe.Steps.Split('~');
            foreach (string x in arraySteps)
            {
                lbl_steps.Text += x + "\n";
            }

            lbl_time.Text = recipe.timeInMinutes.ToString() + " minutes";


        }

    }
}