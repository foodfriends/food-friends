﻿/*
 * Project Name: food Friends
 */

using Xamarin.Forms;
using Xamarin.Forms.Maps;
using Plugin.Geolocator;
using System;
using System.Collections.Generic;
using System.Linq;
using SQLite;

[assembly: Dependency(typeof(foodFriend.foodFriendPage))]
namespace foodFriend
{
	public partial class foodFriendPage : ContentPage
	{
        IEnumerable<Restaurant> rest=null;
        List<Restaurant> rest_list=null;
        private RestaurantDB db_res;
        private RecipeDB db_recipe;
        

        public foodFriendPage()
		{

            InitializeComponent();

			Title = "Food Friends";

            db_res = new RestaurantDB();
            
            //show all restaurants in data
            getAllRestaurant();
        }

        void OnClick(object sender, EventArgs e)
        {
            ToolbarItem tbi = (ToolbarItem)sender;
            this.DisplayAlert("Selected!", tbi.Text, "OK");
        }
        protected override void OnAppearing()
        {
            base.OnAppearing();

            GetLocation();


        }


        //search restaurant by name
        void OnSearchButtonTapped(object sender, EventArgs e)
        {
            MainMap.Pins.Clear();
            var newList = db_res.GetAllRestaurantByName(searchMap.Text);

            foreach (Restaurant res in newList)
            {
                var pin = new Pin
                {
                    Position = new Position(res.Latitude, res.Longtitude),
                    Label = res.Name,
                    Address = res.Address
                };

                MainMap.Pins.Add(pin);

                MainMap.MoveToRegion(MapSpan.FromCenterAndRadius(new Position(res.Latitude, res.Longtitude),
                                             Distance.FromKilometers(0.8)));
            }
        }

        //get the current position 
        private async void GetLocation()
        {
            var locator = CrossGeolocator.Current;
            locator.DesiredAccuracy = 50;
            try
            {
                var position = await locator.GetPositionAsync(timeoutMilliseconds: 1000000);
                MainMap.MoveToRegion(MapSpan.FromCenterAndRadius(new Position(position.Latitude, position.Longitude),
                                             Distance.FromMiles(1)));
            }
            catch (Exception ex)
            {
                await DisplayAlert("GPS Error", "GPS not available on your device", "OK");
            }
            


        }
        
        //traverse the Restaurant
        public void getAllRestaurant() {
            rest = db_res.AroundRestaurant();
            rest_list = rest.ToList();
            for (int i = 0; i < rest_list.Count; i++) {
                

                var pin = new Pin
                {
                    Position = new Position(rest_list[i].Latitude, rest_list[i].Longtitude),
                    Label = rest_list[i].Name,
                    Address = rest_list[i].Address



                };
                pin.Clicked += (object sender, EventArgs args)=>{

                    Restaurant _res = db_res.GetRestaurant(pin.Label);
                    Navigation.PushAsync(new Detail_res(_res));
                 
                };
                MainMap.Pins.Add(pin);
            }
            MainMap.MoveToRegion(new MapSpan(new Position(rest_list[0].Latitude, rest_list[0].Longtitude), 0.1, 0.1));
        }
        

    }
}

