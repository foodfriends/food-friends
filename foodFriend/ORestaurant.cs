﻿/*
 * Project Name: food Friends
 */

using System;
using Microsoft.WindowsAzure.MobileServices;
using Newtonsoft.Json;
namespace foodFriend
{
    public class ORestaurant
    {
        string id;
        string name;
        string address;
        string phone;
        string img_name;
        int ranking;
        string category;
        string description;
        int hour_start;
        int hour_end;
        double latitude;
        double longtitude;
        int dbversion;



        [JsonProperty(PropertyName = "id")]
        public string Id
        {
            get { return id; }
            set { id = value; }
        }

        [JsonProperty(PropertyName = "name")]
        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        [JsonProperty(PropertyName = "address")]
        public string Address
        {
            get { return address; }
            set { address = value; }
        }

        [JsonProperty(PropertyName = "phone")]
        public string Phone
        {
            get { return phone; }
            set { phone = value; }
        }

        [JsonProperty(PropertyName = "img_name")]
        public string Img_Name
        {
            get { return img_name; }
            set { img_name = value; }
        }

        [JsonProperty(PropertyName = "ranking")]
        public int Ranking
        {
            get { return ranking; }
            set { ranking = value; }
        }

        [JsonProperty(PropertyName = "category")]
        public string Category
        {
            get { return category; }
            set { category = value; }
        }

        [JsonProperty(PropertyName = "description")]
        public string Description
        {
            get { return description; }
            set { description = value; }
        }

        [JsonProperty(PropertyName = "hour_start")]
        public int Hour_start
        {
            get { return hour_start; }
            set { hour_start = value; }
        }

        [JsonProperty(PropertyName = "hour_end")]
        public int Hour_end
        {
            get { return hour_end; }
            set { hour_end = value; }
        }

        [JsonProperty(PropertyName = "latitude")]
        public double Latitude
        {
            get { return latitude; }
            set { latitude = value; }
        }

        [JsonProperty(PropertyName = "longtitude")]
        public double Longtitude
        {
            get { return longtitude; }
            set { longtitude = value; }
        }

        public int DBVersion
        {
            get { return dbversion; }
            set { dbversion = value; }
        }
        public enum CategoryType
        {
            Asian = 0,
            Western = 1,
            Australian = 2
        }

        public void setCategory(CategoryType c)
        {
            switch (c)
            {
                case CategoryType.Asian: { Category = "Chinese"; break; }
                case CategoryType.Western: { Category = "Indian"; break; }
                case CategoryType.Australian: { Category = "Pizza"; break; }
                default: { Category = "Unknown"; break; }

            }

        }

        [Version]
        public string Version { get; set; }
    }
}
