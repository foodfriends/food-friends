﻿/*
 * Project Name: food Friends
 */

using System;
using System.Collections.Generic;
using Xamarin.Forms;

namespace foodFriend
{
	public class RecipePage : ContentPage
	{
		private Xamarin.Forms.ListView recipeList;
		RecipeDB db_rest = new RecipeDB();

		public RecipePage()
		{
			// Create StackLayout, one to display list, and the other for search
			var listLayout = new StackLayout() { Orientation = StackOrientation.Vertical, BackgroundColor = Color.FromHex("#fcecdf") };
			var searchArea = new StackLayout() { Orientation = StackOrientation.Horizontal, BackgroundColor = Color.FromHex("#fcecdf") };

			// Set this page's Background Color
			this.BackgroundColor = Color.FromHex("#fcecdf");

			IEnumerable<Recipe> recipe = null;
			Title = "Food Friends";

			// Get recipes from database
			recipe = db_rest.GetAllRecipe();

			// Create and initialise ListView
			recipeList = new Xamarin.Forms.ListView();
			recipeList.ItemsSource = recipe;
			recipeList.ItemTemplate = new DataTemplate(typeof(TextCell));
			recipeList.ItemTemplate.SetBinding(TextCell.TextProperty, "Name");
			recipeList.ItemTemplate.SetBinding(TextCell.DetailProperty, "Category");
			recipeList.BackgroundColor = Color.FromHex("#fcecdf");

			// Set list view's action when its item is tapped, where it will direct to a detail page
			recipeList.ItemTapped += (sender, e) =>
			{

				ListView lv = (ListView)sender;
				Recipe recipeSelected = (Recipe)lv.SelectedItem;
				Navigation.PushAsync(new Detail_recipe(recipeSelected.Name.ToString()));
			};

			// Create and initialise a picker
			string[] categorySearch = new string[] { "Name", "Category"};
			Picker picker = new Picker
			{
				WidthRequest = 100,
				VerticalOptions = LayoutOptions.CenterAndExpand,
			};
			foreach (string categoryName in categorySearch)
			{
				picker.Items.Add(categoryName);
			}
			if (picker.SelectedIndex == -1)
			{
				picker.SelectedIndex = 0;
			}

			// Create and initialise an Entry
			var searchBar = new Entry()
			{
				WidthRequest = 200,
				Placeholder = "Search recipe"
			};

			// Initialise ListView and change its content when searchBar text is changed
			Xamarin.Forms.ListView _searchList = new Xamarin.Forms.ListView();
			List<Recipe> searchResults = new List<Recipe>();
			searchBar.TextChanged += (object sender, TextChangedEventArgs e) =>
			{
				searchResults = new List<Recipe>();
				if (picker.SelectedIndex == 0)
				{
					foreach (Recipe recipeElement in recipe)
					{
						if (recipeElement.Name.ToUpper().Contains(((Entry)sender).Text.ToString().ToUpper()))
						{
							searchResults.Add(recipeElement);
						}
					}
				}
				else if (picker.SelectedIndex == 1)
				{
					foreach (Recipe recipeElement in recipe)
					{
						if (recipeElement.Category.ToUpper().Contains(((Entry)sender).Text.ToString().ToUpper()))
						{
							searchResults.Add(recipeElement);
						}
					}
				}

				_searchList.ItemsSource = searchResults;
				_searchList.ItemTemplate = new DataTemplate(typeof(TextCell));
				_searchList.ItemTemplate.SetBinding(TextCell.TextProperty, "Name");
				_searchList.ItemTemplate.SetBinding(TextCell.DetailProperty, "Category");
				_searchList.BackgroundColor = Color.FromHex("#fcecdf");
				_searchList.ItemTapped += (sender1, e1) =>
				{

					ListView lv = (ListView)sender1;
					Recipe recipeSelected = (Recipe)lv.SelectedItem;
					Navigation.PushAsync(new Detail_recipe(recipeSelected.Name.ToString()));
				};

				if (searchBar.Text == "" || searchBar.Text == null)
				{
					listLayout.Children.Remove(recipeList);
					listLayout.Children.Remove(_searchList);
					listLayout.Children.Add(_searchList);
				}
				else
				{
					listLayout.Children.Remove(recipeList);
					listLayout.Children.Remove(_searchList);
					listLayout.Children.Add(_searchList);
				}
			};

			// Adding UI Widgets to StackLayout
			searchArea.Children.Add(picker);
			searchArea.Children.Add(searchBar);
			listLayout.Children.Add(searchArea);

			if (searchBar.Text == "" || searchBar.Text == null)
			{
				// Add recipeList to listLayout
				listLayout.Children.Add(recipeList);
			}
			else
			{
				// Add _searchList to listLayout
				listLayout.Children.Add(_searchList);
			}

			// Set page's content to listLayout
			Content = listLayout;
		}
	}
}


