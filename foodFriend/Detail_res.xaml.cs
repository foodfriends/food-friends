﻿/*
 * Project Name: food Friends
 */

using System;
using System.Collections.Generic;
using Xamarin.Forms;
using Xamarin.Forms.Maps;
using Plugin.Geolocator;
using System.Threading.Tasks;
using Rg.Plugins.Popup.Extensions;

namespace foodFriend
{
    
    public partial class Detail_res : ContentPage
    {
        private RestaurantDB db_rest;
        private Restaurant restaurant;
        public Detail_res(Restaurant res)
        {
            
            InitializeComponent();
            Title = "Details";
            db_rest = new RestaurantDB();
            restaurant = res;
            getContent();
            
        }
        public void getContent() {
            
            lbl_name.Text = restaurant.Name;
            lbl_address.Text = restaurant.Address;
            lbl_category.Text = restaurant.Category;
            lbl_des.Text = restaurant.Description;
            lbl_phone.Text = restaurant.Phone;
            lbl_open.Text = restaurant.Hour_start + ":00 ~ " + restaurant.Hour_end + ":00";

            var pin = new Pin
            {
                Position = new Position(restaurant.Latitude, restaurant.Longtitude),
                Label = restaurant.Name,
                Address = restaurant.Address
            };

            //Add pins to the map
            MainMap.Pins.Add(pin);
            MainMap.MoveToRegion(new MapSpan(new Position(restaurant.Latitude, restaurant.Longtitude), 0.005, 0.005));
            //loading the photo for restaurant detail page
            stack_photo.Children.Clear();
            Image image = new Image()
			{
				Source = restaurant.Img_Name
			};

			var tapGestureRecognizer = new TapGestureRecognizer();
			tapGestureRecognizer.Tapped += async (sender, e) =>
			{
				image.Opacity = .5;
				await Task.Delay(100);
				image.Opacity = 1;
				var page = new PopUpImage(restaurant.Img_Name);
				await Navigation.PushPopupAsync(page);
			};

            //Add tapping gesture to the image on details page
			image.GestureRecognizers.Add(tapGestureRecognizer);
			stack_photo.Children.Add(image);


        }
    }
}