﻿/*
 * Project Name: food Friends
 */

using System;
using SQLite;
using Xamarin.Forms;

namespace foodFriend
{
    public class Recipe
    {
        [PrimaryKey, AutoIncrement]
        public int ID { get; set; }
        public string Name { get; set; }

        public string Category { get; set; }
        public string Ingredients { get; set; }
        public string Steps { get; set; }
        public int timeInMinutes { get; set; }

        public enum CategoryType
        {
            Asian = 0,
            Western = 1,
            Australia = 2
        }

        public void setCategory(CategoryType c)
        {
            switch (c)
            {
                case CategoryType.Asian: { Category = "Asian"; break; }
                case CategoryType.Western: { Category = "Western"; break; }
                case CategoryType.Australia: { Category = "Australian"; break; }
                default: { Category = "Unknown"; break; }

            }

        }
        public string[] GetSteps()
        {
            string[] steps = Steps.Split('~');
            return steps;
        }

        public string[] GetIngredients()
        {
            string[] ingredients = Ingredients.Split('~');
            return ingredients;
        }
    }
}
