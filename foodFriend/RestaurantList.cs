﻿/*
 * Project Name: food Friends
 */

using System;
using System.Collections.Generic;
using Xamarin.Forms;

namespace foodFriend
{
	public class RestaurantList : ContentPage
	{
		private Xamarin.Forms.ListView _restaurantList;
		RestaurantDB db_rest = new RestaurantDB();

		public RestaurantList()
		{
			// Create StackLayout, one to display list, and the other for search
			var listLayout = new StackLayout() { Orientation = StackOrientation.Vertical, BackgroundColor = Color.FromHex("#fcecdf") };
			var searchArea = new StackLayout() { Orientation = StackOrientation.Horizontal, BackgroundColor = Color.FromHex("#fcecdf") };

			// Set this page's Background Color
			this.BackgroundColor = Color.FromHex("#fcecdf");

			IEnumerable<Restaurant> restaurant = null;
			Title = "Food Friends";

			//if restaurant is not initialised in the previous page, then get restaurant from database
			if (restaurant == null) restaurant = db_rest.GetRestaurant();

			// Create and initialise ListView
			_restaurantList = new Xamarin.Forms.ListView();
			_restaurantList.ItemsSource = restaurant;
			_restaurantList.ItemTemplate = new DataTemplate(typeof(TextCell));
			_restaurantList.ItemTemplate.SetBinding(TextCell.TextProperty, "Name");
			_restaurantList.ItemTemplate.SetBinding(TextCell.DetailProperty, "Category");
			_restaurantList.BackgroundColor = Color.FromHex("#fcecdf");

			// Set list view's action when its item is tapped, where it will direct to a detail page
			_restaurantList.ItemTapped += (sender, e) => {

                ListView lv = (ListView)sender;
                Restaurant _res = (Restaurant)lv.SelectedItem;
                Navigation.PushAsync(new Detail_res(_res));
            };

			// Create and initialise a picker
			string[] categorySearch = new string[]{ "Name", "Category", "Address"} ;
			Picker picker = new Picker
			{
				WidthRequest = 100,
				VerticalOptions = LayoutOptions.CenterAndExpand,
			} ;

			foreach (string categoryName in categorySearch)
			{
				picker.Items.Add(categoryName);
			}

			if (picker.SelectedIndex == -1)
			{
				picker.SelectedIndex = 0;
			}

			// Create and initialise an Entry
			var searchBar = new Entry()
			{
				WidthRequest = 200,
				Placeholder = "Search restaurant"
			} ;

			// Initialise ListView and change its content when searchBar text is changed
			Xamarin.Forms.ListView _searchList = new Xamarin.Forms.ListView();
			List<Restaurant> searchResults = new List<Restaurant>();
			searchBar.TextChanged += (object sender, TextChangedEventArgs e) =>
			{
				searchResults = new List<Restaurant>();
				if (picker.SelectedIndex == 0)
				{
					foreach (Restaurant restaurantElement in restaurant)
					{
						if (restaurantElement.Name.ToUpper().Contains(((Entry)sender).Text.ToString().ToUpper()))
						{
							searchResults.Add(restaurantElement);
						}
					}
				}	
				else if (picker.SelectedIndex == 1)
				{
					foreach (Restaurant restaurantElement in restaurant)
					{
						if (restaurantElement.Category.ToUpper().Contains(((Entry)sender).Text.ToString().ToUpper()))
						{
							searchResults.Add(restaurantElement);
						}
					}
				}
				else if (picker.SelectedIndex == 2)
				{
					foreach (Restaurant restaurantElement in restaurant)
					{
						if (restaurantElement.Address.ToUpper().Contains(((Entry)sender).Text.ToString().ToUpper()))
						{
							searchResults.Add(restaurantElement);
						}
					}
				}

				_searchList.ItemsSource = searchResults;
				_searchList.ItemTemplate = new DataTemplate(typeof(TextCell));
				_searchList.ItemTemplate.SetBinding(TextCell.TextProperty, "Name");
				_searchList.ItemTemplate.SetBinding(TextCell.DetailProperty, "Category");
				_searchList.BackgroundColor = Color.FromHex("#fcecdf");
                _searchList.ItemTapped += (sender1, e1) => {

                    ListView lv = (ListView)sender1;
                    Restaurant _res = (Restaurant)lv.SelectedItem;
                    Navigation.PushAsync(new Detail_res(_res));
                };

                if (searchBar.Text == "" || searchBar.Text == null)
				{
					listLayout.Children.Remove(_restaurantList);
					listLayout.Children.Remove(_searchList);
					listLayout.Children.Add(_searchList);
				}
				else
				{
					listLayout.Children.Remove(_restaurantList);
					listLayout.Children.Remove(_searchList);
					listLayout.Children.Add(_searchList);
				}
			} ;

			// Adding UI Widgets to StackLayout
			searchArea.Children.Add(picker);
			searchArea.Children.Add(searchBar);
			listLayout.Children.Add(searchArea);

			if (searchBar.Text == "" || searchBar.Text == null)
			{
				// Add _restaurantList to listLayout
				listLayout.Children.Add(_restaurantList);
			}
			else
			{
				// Add _searchList to listLayout
				listLayout.Children.Add(_searchList);
			}

			// Set page's content to listLayout
			Content = listLayout;
		}

	}
}



