﻿/*
 * Project Name: food Friends
 */

using System;
using System.Collections.Generic;
using System.Linq;
using SQLite;
using Xamarin.Forms;
[assembly: Dependency(typeof(foodFriend.RecipeDB))]


namespace foodFriend
{
    public class RecipeDB
    {
        

        public RecipeDB()
        {
            SQLiteConnection _connection;
            ISQLite iSqlite = DependencyService.Get<ISQLite>();
            _connection = iSqlite.GetConnection();
            
            if (!TableExists("Recipe"))
            {
                _connection.CreateTable<Recipe>();
                _connection.Close();
                InitialiseDB();
            }
            else _connection.Close();



        }

        public Boolean TableExists(String tableName)
        {
            SQLiteConnection _connection;
            ISQLite iSqlite = DependencyService.Get<ISQLite>();
            _connection = iSqlite.GetConnection();

            SQLite.TableMapping map = new TableMapping(typeof(Recipe)); // Instead of mapping to a specific table just map the whole database type
            object[] ps = new object[0]; // An empty parameters object since I never worked out how to use it properly! (At least I'm honest)
            Int32 tableCount = _connection.Query(map, "SELECT * FROM sqlite_master WHERE type = 'table' AND name = '" + tableName + "'", ps).Count; // Executes the query from which we can count the results
            if (tableCount == 0)
            {
                _connection.Close();
                return false;
            }
            else if (tableCount == 1)
            {
                _connection.Close();
                return true;
            }
            else
            {
                _connection.Close();
                throw new Exception("More than one table by the name of " + tableName + " exists in the database.", null);
            }
        }
        public IEnumerable<Recipe> GetAllRecipe()
        {
            SQLiteConnection _connection;
            ISQLite iSqlite = DependencyService.Get<ISQLite>();
            _connection = iSqlite.GetConnection();
            IEnumerable<Recipe> recipe = _connection.Query<Recipe>("SELECT * FROM [Recipe]");
            _connection.Close();
            return recipe;
        }
        public IEnumerable<Recipe> GetRecipeByType(Recipe.CategoryType ctgr)
        {
            SQLiteConnection _connection;
            ISQLite iSqlite = DependencyService.Get<ISQLite>();
            _connection = iSqlite.GetConnection();
            IEnumerable<Recipe> recipe = _connection.Query<Recipe>("SELECT * FROM [Recipe] where Category = ?", ctgr.ToString());
            return recipe;
        }
        public void DropTable()
        {
            SQLiteConnection _connection;
            ISQLite iSqlite = DependencyService.Get<ISQLite>();
            _connection = iSqlite.GetConnection();
            _connection.DropTable<Recipe>();
            _connection.Close();
        }

        

        public Recipe GetRecipe(int id)
        {
            SQLiteConnection _connection;
            ISQLite iSqlite = DependencyService.Get<ISQLite>();
            _connection = iSqlite.GetConnection();
            Recipe recipe = _connection.Table<Recipe>().FirstOrDefault(t => t.ID == id);
            _connection.Close();
            return recipe;

        }
        public Recipe GetRecipe(string name)
        {
            SQLiteConnection _connection;
            ISQLite iSqlite = DependencyService.Get<ISQLite>();
            _connection = iSqlite.GetConnection();
            Recipe recipe = _connection.Table<Recipe>().FirstOrDefault(t => t.Name == name);
            return recipe;
        }


        public void DeleteRecipe(int id)
        {
            SQLiteConnection _connection;
            ISQLite iSqlite = DependencyService.Get<ISQLite>();
            _connection = iSqlite.GetConnection();
            _connection.Delete<Recipe>(id);
            _connection.Close();
        }

        public void AddRecipe(string name, Recipe.CategoryType type, string ingredients, string steps, int time)
        {
            SQLiteConnection _connection;
            ISQLite iSqlite = DependencyService.Get<ISQLite>();
            _connection = iSqlite.GetConnection();
            var recipe = new Recipe
            {
                Name = name,
                Category = type.ToString(),
                Ingredients = ingredients,
                Steps = steps,
                timeInMinutes = time
            };
            _connection.Insert(recipe);
            _connection.Close();
        }

        public void InitialiseDB()
        {
            AddRecipe("Macaroni Cheese", Recipe.CategoryType.Western,
                "400g dried macaroni pasta.~375ml can Carnation Evaporated Milk.~150g Mersey Valley Vintage Club Spreadable cheese - Original.~3 large shallots, trimmed, thinly sliced.",
                "1. Cook the pasta in a large saucepan of salted boiling water following packet directions.~2.Meanwhile, place the evaporated milk and cheddar in a saucepan over medium - low heat.Cook, stirring, for 3 - 4 minutes or until the cheddar melts and the mixture is smooth.Season with salt and pepper.Add the shallot and cook, stirring, for 1 minute.~3.Drain the pasta and return to the pan. Pour over the sauce and toss to combine. Season with pepper.", 30);

            AddRecipe("Fish Burger", Recipe.CategoryType.Western,
                "4 frozen crumbed fish fillets.~2 small Lebanese cucumbers.~8 butter lettuce leaves.~1 small avocado, sliced. ~4 long white bread rolls, split, toasted.~1/3 cup whole-egg aioli.",
                "1. Preheat oven to 190ºC/170ºC fan-forced. Place fish fillets on a non-stick baking tray. Bake for 20 to 25 minutes, turning halfway during cooking, or until cooked through.~2.Using a vegetable peeler, peel cucumbers into ribbons.~3.Divide lettuce and avocado between roll bases.Top with fish, cucumber and aioli.Top with roll tops.Serve.", 35);

            AddRecipe("Chocolate Lava Cakes", Recipe.CategoryType.Western,
                "180g dark chocolate, chopped. ~250g butter, chopped.~plain flour, for dusting.~4 eggs.1/2 cup caster sugar.~1/4 cup plain flour, sifted.~Double cream and cocoa powder, to serve.",
                "1. Place chocolate and butter in a microwave-safe bowl. Microwave on medium-high (75%) for 1 to 2 minutes, stirring with a metal spoon every 30 seconds, or until melted and combined. Cool completely.~2.Grease eight 175ml - capacity ceramic ovenproof dishes.Sprinkle with flour.Using an electric mixer, beat eggs and sugar for 8 to 10 minutes or until thick and creamy.Fold in chocolate mixture, then flour.Spoon mixture into prepared dishes.Freeze for 1 hour.~3.Preheat oven to 200°C / 180°C fan - forced.Place dishes on a baking tray.Bake for 16 minutes or until just set(cakes will wobble when touched).~4.Stand dishes for 1 minute.Turn onto plates.Top with cream.Dust with cocoa powder.Serve.", 93);


            AddRecipe("Chili prawn and tomato spaghetti", Recipe.CategoryType.Western,
                "375g dried spaghetti pasta.~1 tablespoon extra-virgin.~Olive oil.~4 garlic cloves, thinly sliced.~1/4 teaspoon dried chili flakes.~20 (650g) medium green prawns, peeled (tails intact), deveined.~6 medium tomatoes, deseeded, finely chopped.~2 tablespoons chopped fresh flat-leaf parsley leaves.",
                "1. Cook pasta in a large saucepan of boiling, salted water following packet directions, until tender. Drain, reserving 1/2 cup cooking liquid. Return pasta to pan.~2.Meanwhile, heat oil in a large frying pan over medium - high heat.Add garlic and chili.Cook for 1 minute or until fragrant.Add prawns.Cook, stirring, for 2 to 3 minutes or until pink and cooked through.Add tomato and cooking liquid.Cook for 2 minutes or until heated through.Season with salt and pepper.Stir through parsley.Serve.", 30);


            AddRecipe("Steak with ratatouille", Recipe.CategoryType.Western,
                "1/4 cup olive oil.~1 brown onion, chopped.~1 large eggplant, cut into 1cm cubes.~1 large red capsicum, diced.~400g can chopped tomatoes with basil.~1 tablespoon balsamic vinegar.~4 (200g each) scotch fillet steaks, trimmed.~800g boiled chat potatoes, to serve.",
                "1. Heat 1 tablespoon of oil in a non-stick frying pan over medium heat. Add onion. Cook, stirring, for 4 minutes or until tender. Add eggplant and capsicum. Cook for 10 minutes or until eggplant is tender. Add tomatoes and 1/4 cup cold water. Cook for 4 minutes or until sauce thickens slightly. Season with salt and pepper.~2.Meanwhile, combine vinegar and 1 tablespoon of oil in a ceramic dish.Add steaks and turn to coat.Preheat barbecue plate or chargrill pan over high heat.Cook steaks, basting with marinade, for 3 to minutes each side for medium or until cooked to your liking.Transfer to a plate.Cover with foil.Allow to rest for 5 minutes.~3.Toss potatoes in remaining oil.Season with salt and pepper.Place steaks on plates.Top with ratatouille.Serve with potatoes.", 40);


            AddRecipe("Charred Beef with Couscous", Recipe.CategoryType.Australia,
                "4x 350g beef sirloin steaks, trimmed.~ 1½ tsp ground cumin.~ 1½ tsp ground coriander.~ 1½ tsp garlic powder (or you can use crushed garlic).~ 1 lime, zested and juiced.~ ⅓ cup The Olive Tree olive oil.~ 500g butternut pumpkin, seeds removed, cut into rough wedges.~ 2 bunches broccolini, trimmed.~ Herb couscous.~ 1½ tbsp olive oil.~ 1 cup couscous.~ 1 cup vegetable stock.~ 1 lemon, finely zested, juiced.~ 2 tbsp finely chopped flat-leaf parsley.~ 2 tbsp finely chopped mint.~ To serve.~ Light sour cream.~ Lime wedges",
                "1 Place spices, garlic, zest, juice and 2 tablespoons in a large bowl and whisk to combine. Season to taste. Add steak and coat well in marinade. Cover and marinate at room temperature for 30 minutes. ~2 Preheat oven to 200°C.Meanwhile, place pumpkin on a large shallow oven tray and drizzle with remaining oil and season to taste.Roast for 25 minutes or until golden and cooked.Bring a large saucepan of boiling water to the boil, add brocolini and cook for 5 minutes or until cooked to your liking.Drain and season to taste.~3 Preheat a lightly oiled char - grill pan or barbecue to high.Cook steak for 2½ -3 minutes each side or until cooked to your liking.Rest for 15 minutes, covered with foil in a warm place, before slicing, to serve~4 To make herbed couscous, place couscous in a large heatproof bowl and stir through olive oil.Bring stock to the boil and stir into couscous.Cover and stand for 10 minutes.Using a fork, gradually loosen couscous then stir in remaining ingredients.Season to taste.~5 Divide couscous, brocolini, pumpkin and flank steak among plates.Serve with lime wedges.",
            45);

            AddRecipe("Mushroom & Soft Cheese Pancakes ", Recipe.CategoryType.Australia,
                "400g white mushrooms.~200g cream cheese.~40g Beautifully Butterfully butter.~1 Chefs Cupboard vegetable stock cube.~1 tsp Stonemill paprika.~100g White Mill plain flour.~2 Lodge Farms free range eggs.~250ml Farmdale milk.~2 tbs dried parsley.~½ tsp white pepper.~Sea salt for seasoning.~A little vegetable oil.~23 - 24cm frying pan ",
                "1.Wipe the mushrooms and chop.~2 Melt the butter in the frying pan, saut the mushrooms for 5 - 6 minutes, crumble over the stock cube, paprika and add the soft cheese, cook gently for a few minutes stirring as you cook – remove from pan and allow to cool.~3 To make the pancakes – sift the flour in a bowl then add the parsley, pepper and some sea salt.~4 Crack the eggs in a jug with the milk and whisk well.~5 Add this to the flour, whisking as you mix until you have a smooth batter.~6 Pre - heat the oven to 180°C(or 160°C fan - forced).~7 Wash the frying pan – using some oil in the pan, make 6 pancakes.Put the pancakes out flat, divide the mixture between them, putting it in the middle of the pancakes, fold up the sides to make a square.~8 Bake in the oven for 10 minutes and serve.~9 You can make these ahead of time – and then heat up in the oven when ready – they will take a little extra time to re - heat if they have been in the fridge.",
            50);

            AddRecipe("Cheddar Cheese and Broccoli Soup", Recipe.CategoryType.Australia,
                "190g Westacre Tasty Cheese*, grated.~25g Beautifully Butterfully butter.~1 Medium onion – chopped.~1 Bunch fresh broccoli.~150g Potatoes – peeled.~1 x Chef’s Cupboard Vegetable Stock cube.~800ml Water.~300ml Fardmale cream.~Salt and black pepper",
                "1 Melt the butter in a large saucepan~2 Add the onion and sauté until soft~3 Chop up the broccoli and the potato and add to the pan, cook stirring for a few minutes.Add the water and the stock cube, season with salt and pepper,~4 Bring to the boil, cover with a lid and simmer for about 20 / 30 minutes until the vegetables are tender.~5 Let soup cool slightly, liquidise the soup.~6 Add the cream, check the seasoning and then stir in the grated cheese, cook gently until the cheese has melted ",
                50);

            AddRecipe("Gluten Free Citrus Pudding", Recipe.CategoryType.Australia,
                  "90g butter or monounsaturated margarine.~¾ cup Merryfield caster sugar.~3 Lodge Farm eggs, separated.~½ tsp vanilla.~45g Has No Gluten Free Self Raising Flour.~1½ cups Farmdale milk.~finely grated rind 1 lemon.~finely grated rind 1 orange.~¼ cup lemon juice (approx. 1 lemon).~¼ cup orange juice (approx. 1 orange)",
                  "1 Preheat oven to 160°C (325°F). ~2 Prepare a medium - sized casserole dish by spraying with cooking spray.~3 Cream butter, sugar, eggs yolk and vanilla.Stir in flour, milk, lemon juice, orange juice and lemon and orange rind.~4 Place egg whites into a clean, dry bowl.Beat until stiff.Lightly fold into lemon mixture.~5 Pour into prepared dish.~6 Place into a moderate oven and bake for approx. 35 minutes or until cooked when tested.",
                  35);

            AddRecipe("Summer Crush Ice Blocks",
                  Recipe.CategoryType.Australia,
                  "1 punnet strawberries, sliced.~2 kiwi fruit. 1 cup seedless watermelon, in large pieces. 1 cup Country Orchard Premium Orange Juice~Ice block moulds.~6 x clean paddle pop sticks",
                  "1 Peel and dice kiwi fruit. Divide strawberries and kiwi fruit between moulds. ~2 In a blender, blend watermelon and orange juice until no lumps remain.~3 Pour orange and watermelon juice over fruit.Insert paddle pop sticks and freeze until set.",
                  90);

        }
    }
}
