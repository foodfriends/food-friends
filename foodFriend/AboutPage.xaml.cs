﻿/*
 * Project Name: food Friends
 */

using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace foodFriend
{
	public partial class AboutPage : ContentPage
	{
		public AboutPage()
		{
			InitializeComponent();

			// Set page's title
			Title = "About Food Friends";
		}
	}
}

