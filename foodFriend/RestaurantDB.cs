﻿/*
 * Project Name: food Friends
 */

using System;
using System.Collections.Generic;
using System.Linq;
using SQLite;
using Xamarin.Forms;
using System.Threading.Tasks;

[assembly: Dependency(typeof(foodFriend.RestaurantDB))]


namespace foodFriend
{
    public class RestaurantDB
    {

        public RestaurantDB()
        {
            SQLiteConnection _connection;
            ISQLite iSqlite = DependencyService.Get<ISQLite>();
            _connection = iSqlite.GetConnection();
            if (!TableExists("Restaurant"))
            {
                _connection.CreateTable<Restaurant>();
                InitialiseDB();
            }
            _connection.Close();

        }

        //check if an table already exists
        public Boolean TableExists(String tableName)
        {
            SQLiteConnection _connection;
            ISQLite iSqlite = DependencyService.Get<ISQLite>();
            _connection = iSqlite.GetConnection();

            SQLite.TableMapping map = new TableMapping(typeof(Restaurant)); // Instead of mapping to a specific table just map the whole database type
            object[] ps = new object[0]; // An empty parameters object since I never worked out how to use it properly! (At least I'm honest)
            Int32 tableCount = _connection.Query(map, "SELECT * FROM sqlite_master WHERE type = 'table' AND name = '" + tableName + "'", ps).Count; // Executes the query from which we can count the results
            if (tableCount == 0)
            {
                _connection.Close();
                return false;
            }
            else if (tableCount == 1)
            {
                _connection.Close();
                return true;
            }
            else
            {
                _connection.Close();
                throw new Exception("More than one table by the name of " + tableName + " exists in the database.", null);
            }

        }

        //returns restaurants around the users
        public IEnumerable<Restaurant> AroundRestaurant()
        {
            SQLiteConnection _connection;
            ISQLite iSqlite = DependencyService.Get<ISQLite>();
            _connection = iSqlite.GetConnection();
            IEnumerable<Restaurant> res = _connection.Query<Restaurant>("SELECT * FROM [Restaurant]");
            _connection.Close();
            return res;

        }

        //drop table restaurant
        public void DropTable()
        {
            SQLiteConnection _connection;
            ISQLite iSqlite = DependencyService.Get<ISQLite>();
            _connection = iSqlite.GetConnection();
            _connection.DropTable<Restaurant>();
            _connection.Close();
        }
        //get all restaurants
        public IEnumerable<Restaurant> GetRestaurant()
        {
            SQLiteConnection _connection;
            ISQLite iSqlite = DependencyService.Get<ISQLite>();
            _connection = iSqlite.GetConnection();
            IEnumerable<Restaurant> res = (from t in _connection.Table<Restaurant>()
                                           select t).ToList();
            _connection.Close();
            return res;

        }
        //get restaurant by Id
        public Restaurant GetRestaurant(int id)
        {
            SQLiteConnection _connection;
            ISQLite iSqlite = DependencyService.Get<ISQLite>();
            _connection = iSqlite.GetConnection();
            Restaurant res = _connection.Table<Restaurant>().FirstOrDefault(t => t.ID == id);
            _connection.Close();
            return res;
        }
        //get restaurant by name
        public Restaurant GetRestaurant(string name)
        {
            SQLiteConnection _connection;
            ISQLite iSqlite = DependencyService.Get<ISQLite>();
            _connection = iSqlite.GetConnection();
            Restaurant res = _connection.Table<Restaurant>().FirstOrDefault(t => t.Name == name);
            _connection.Close();
            return res;
        }
        //like searching restaurants
        public IEnumerable<Restaurant> GetAllRestaurantByName(string name)
        {
            SQLiteConnection _connection;
            ISQLite iSqlite = DependencyService.Get<ISQLite>();
            _connection = iSqlite.GetConnection();
            return _connection.Query<Restaurant>("SELECT * FROM [Restaurant] where Name like '%" + name + "%'");

        }

        //delete restaurant by Id
        public void DeleteRestaurant(int id)
        {
            SQLiteConnection _connection;
            ISQLite iSqlite = DependencyService.Get<ISQLite>();
            _connection = iSqlite.GetConnection();
            _connection.Delete<Restaurant>(id);
            _connection.Close();

        }
        //a method for insert new restaurants
        public void AddRestaurant(String name, String address, String phone, String img, int ranking, Restaurant.CategoryType category, string des, int hour_start, int hour_end, double longtitude, double latitude)
        {
            SQLiteConnection _connection;
            ISQLite iSqlite = DependencyService.Get<ISQLite>();
            _connection = iSqlite.GetConnection();
            var res = new Restaurant
            {
                Name = name,
                Address = address,
                Phone = phone,
                Img_Name = img,
                Ranking = ranking,
                Category = category.ToString(),
                Description = des,
                Hour_start = hour_start,
                Hour_end = hour_end,
                Longtitude = longtitude,
                Latitude = latitude,
                DBVersion = 1
            };
            _connection.Insert(res);
            _connection.Close();
        }
        //initialising the lcoal database at the first time run of this app
        //local database will be udpdated if there's any new version and the internet is available
        public void InitialiseDB()
		{
			AddRestaurant("Longrain Melbourne", "44 Little Bourke Street, CBD, Melbourne, VIC",
				"03 86578 528", "longrain.png", 1, Restaurant.CategoryType.Asian,
				"Longrain is a restaurant located in the CBD of Melbourne.The menu was inspired by Thai food.This restaurant fits perfectly for those who eats in group as servings are large.",
				18, 22, 144.971210, -37.810648);

			AddRestaurant("Vanilla Bean", "287 Exhibition Street, CBD, Melbourne",
				"03 9663 0010", "vanillabean.png", 2, Restaurant.CategoryType.Australian,
				"Located in the CBD, this restaurant’s serves Australian cuisines.Our recommended dish is Avocado & Fritters here.",
				9, 21, 144.9689167, -37.8090647);

			AddRestaurant("Gingerboy", "27 - 29 Crossley Street, CBD, Melbourne, VIC",
				"03 8592 7395", "gingerboy.png", 3, Restaurant.CategoryType.Asian,
				"Gingerboy is a unique restaurant that serves Tapas.Its well known for the atmosphere and its South East Asian food.", 12, 14, 144.9709028, -37.8112301);

			AddRestaurant("Mitre Tavern", "5 Bank Place, CBD, Melbourne",
				"03 9602 5611", "mitretavern.png", 4, Restaurant.CategoryType.Western,
				"A restaurant located in CBD, Melbourne.Mitre Tavern is a place for fine dining, facilitated with live music, Wi - Fi, and full bar.Known best for its quality of steak.", 12, 22, 144.9603447, -37.8167782);

			AddRestaurant("Dragon Boat", "203 Little Bourke Street, CBD, Melbourne",
				"03 8592 4948", "dragonboat.png", 5, Restaurant.CategoryType.Asian,
				"A Chinese restaurant located at the central business district of Melbourne.Open from morning until late at night, a perfect place to have Asian food.", 8, 23, 144.9666626, -37.8121126);

			AddRestaurant("Sosta Cucina", "12 Errol Street, Melbourne, Victoria, Australia",
				"03 9329 2882", "sostacucina.png", 6, Restaurant.CategoryType.Western,
				"Sosta Cucina is an Italian restaurant located at North Melbourne.The restaurant is especially good for business meetings, special occasion dining, and romantic.Customer satisfaction of the restaurant is quite good, so it’s a recommended restaurant.", 12, 22, 144.9495684, -37.8048261);

			AddRestaurant("Hot Poppy", "9 Errol St, Melbourne, Victoria 3051, Australia",
				"03 9326 9299", "hotpoppy.png", 7, Restaurant.CategoryType.Australian,
				"A café with a good food, service, value, and atmosphere.Located in North of Melbourne, this café is good for eating breakfast or lunch.", 7, 17, 144.949047, -37.804762);

			AddRestaurant("Albion Hotel", "171 Curzon St | North Melbourne, Melbourne, Victoria 3051, Australia",
						  "03 9326 6575", "albionhotel.png", 8, Restaurant.CategoryType.Australian,
				"Place for those who wants to eat lunch or dinner.Located in North Melbourne, there’s also bar and pub here.", 18, 23, 144.9485744, -37.798879);

			AddRestaurant("Akita", "Cnr Courtney & Blackwood Streets, Melbourne, Victoria 3051, Australia",
				"03 9326 5766 ", "akita.png", 9, Restaurant.CategoryType.Asian,
				"Sushi Lovers ? It’s a place for you.For those who like Japanese food and Sushi, without a doubt, this is the right place for you to go.", 12, 22, 144.9540952, -37.8020156);

			AddRestaurant("The Leveson", "46 Leveson St, Melbourne, Victoria 3051, Australia",
				"03 9328 1109", "theleveson.png", 10, Restaurant.CategoryType.Australian,
				"Looking for a bar or pub ? You can go to The Leveson.A location that is nice with a friendly staff, making this restaurant feel like a 5 - star restaurant.", 12, 23, 144.9512688, -37.8037112);

			AddRestaurant("Chez Dre", "Rear of 285 / 287 Coventry St | Access Via Alleyway, South Melbourne, Port Phillip, Victoria 3205, Australia",
				"03 9690 2688", "chezdre.png", 11, Restaurant.CategoryType.Western,
				"Chez Drew serves French and Australian dishes, with gluten free options.", 7, 16, 144.963937, -37.833889);

			AddRestaurant("Hercules Morse Kitchen + Bar", "283 Clarendon St, South Melbourne, Port Phillip, Victoria 3205, Australia",
				"0396909402", "hercules.png", 12, Restaurant.CategoryType.Australian,
				"Hercules Morse Kitchen is a restaurant that got a bar and serves Australian as well as International dishes.Also, the food has gluten free options.", 12, 23, 144.960959, -37.833236);

			AddRestaurant("Peko Peko", "190 Wells St | Southbank, South Melbourne, Port Phillip, Victoria 3205, Australia",
				"96861109", "pekopeko.png", 13, Restaurant.CategoryType.Asian,
				"An Asian restaurant serving Chinese and Taiwanese food located in South Australia. People love the value and food being sold. Also, this restaurant is child friendly.",
				12, 21, 144.969504, -37.830649);
			AddRestaurant("Simply Spanish", "116 Cecil St, South Melbourne, Port Phillip, Victoria 3205, Australia",
				"0396826100", "simplyspanish.png", 14, Restaurant.CategoryType.Western,
				"Mediterranean, Spanish, and Latin food are all served here. Those who wants to eat the food can get it from morning until night, since the opening hours is from 8am to 10pm.",
				8, 22, 144.957196, -37.832473);
			AddRestaurant("Big Huey's Diner", "315 Coventry St | Across the Road From South Melbourne Market, South Melbourne, Port Phillip, Victoria 3205, Australia",
				"03 9686 1122", "bighuey.png", 15, Restaurant.CategoryType.Western,
				"Love burgers? Yes, Big Huey’s Diner makes a delicious burger and many more American foods you love. Located in South Melbourne, you can buy foods from here during breakfast, brunch, lunch, and dinner time.",
				12, 21, 144.963937, -37.833889);
			AddRestaurant("Gertrude Street Enoteca", "229 Gertrude Street, Fitzroy, Melbourne",
				"03 9415 8262", "gertrude.png", 16, Restaurant.CategoryType.Western,
				"Gertrude Street Enoteca is a restaurant that serves Western cuisine.  This restaurant is available at East Melbourne. The highlights of this restaurant includes gluten free options, outdoor seating, and vegetable friendly.",
				11, 23, 144.982188, -37.806129);

			AddRestaurant("Geppetto Trattoria", "78A Wellington Parade, East Melbourne, Melbourne",
				"03 9417 4691", "geppetto.png", 17, Restaurant.CategoryType.Western,
				"Geppetto Trattoria is a restaurant that is placed at East Melbourne that serves a delicious Italian cuisine. This restaurant also got a full bar.",
				12, 23, 144.987416, -37.816419);
			AddRestaurant("My Sushi", "154 Wellington Parade, East Melbourne, Melbourne",
				"03 9419 0095", "mysushi.png", 18, Restaurant.CategoryType.Asian,
				" Japanese restaurant that is placed at East Melbourne. Love Japanese Food? You can get it here.",
				11, 20, 144.984947, -37.816106);

			AddRestaurant("Pho 365", "24 Smith Street, Collingwood, Melbourne",
				"03 9417 1881", "pho365.png", 19, Restaurant.CategoryType.Asian,
				"Pho is the main specialty of Pho 365. This restaurant is located at East Melbourne, and beside Pho, they also sell rice paper roll, rice vermicelli, and spring rolls.",
				10, 15, 144.982990, -37.807503);
			AddRestaurant("Mina-no-ie", "33 Peel Street, Collingwood, Melbourne, VIC",
				"03 9417 7749", "minanoie.png", 20, Restaurant.CategoryType.Asian,
				"A restaurant that suits for eating lunch or breakfast. Specialised on Japanese food, foods here have the Japanese taste that eater will like.",
				08, 16, 144.985603, -37.805408);

			AddRestaurant("In Season Thai Cuisine", "279 Victoria Street, West Melbourne, Melbourne",
				"03 9328 2021", "inseasonthai.png", 21, Restaurant.CategoryType.Asian,
				"A Thai restaurant located in West Melbourne that serves Thai entrée, soup, salad and grills.  Its highlights are delivery, takeaway, and eat at the restaurant.",
				17, 22, 144.951976, -37.805643);
			AddRestaurant("Biryani House", "343 King Street, West Melbourne, Melbourne",
				"03 9329 4323", "biryani.png", 22, Restaurant.CategoryType.Asian,
				"Love Indian food? Then, Biryani House will be the restaurant that you will like. Serving dishes like rice dishes, lamb dishes, vegetarian dishes, chicken dishes, seafood dishes, snacks, and accompaniments, making this restaurant full of choices.",
				12, 22, 144.953557, -37.812244);
			AddRestaurant("Le Taj", "74 Rosslyn Street, West Melbourne, Melbourne",
				"03 9329 8402", "letaj.png", 23, Restaurant.CategoryType.Asian,
				"Indian food is really popular, and Le Taj sells Indian food. Available at West Melbourne, this restaurant’s specialty are Dal Makhani, Kadhai Chicken, Beef Madras, Bhuna Lamb, and Paneer Kadhai Wala.",
				12, 22, 144.952021, -37.807744);


		}
        //get the current version of local database to compare with the online database
        public int GetCurrentVersion()
        {
            SQLiteConnection _connection;
            ISQLite iSqlite = DependencyService.Get<ISQLite>();
            _connection = iSqlite.GetConnection();
            Restaurant res = _connection.Table<Restaurant>().FirstOrDefault();
            _connection.Close();

            if (res != null) return res.DBVersion;
            // else this mean there nothing in the local DB
            else return -1;
        }

        //updating the local database if its version is behind the onlnie database
        public async Task UpdateDB(IEnumerable<ORestaurant> res, int version)
        {
            if (res != null && res.Count() > 0)
            {
                SQLiteConnection _connection;
                ISQLite iSqlite = DependencyService.Get<ISQLite>();
                _connection = iSqlite.GetConnection();
                //faster way of clear table
                _connection.DropTable<Restaurant>();
                _connection.CreateTable<Restaurant>();

                //insert all new weekly restaurants
                foreach (ORestaurant online_res in res)
                {
                    Restaurant local_res = new Restaurant()
                    {
                        Name = online_res.Name,
                        Address = online_res.Address,
                        Phone = online_res.Phone,
                        Img_Name = online_res.Img_Name,
                        Ranking = online_res.Ranking,
                        Category = online_res.Category,
                        Description = online_res.Description,
                        Hour_start = online_res.Hour_start,
                        Hour_end = online_res.Hour_end,
                        Longtitude = online_res.Longtitude,
                        Latitude = online_res.Latitude,
                        DBVersion = version
                    };
                    _connection.Insert(local_res);
                };
                _connection.Close();
            }

        }

        public IEnumerable<Restaurant> GetTopTen()
        {
            SQLiteConnection _connection;
            ISQLite iSqlite = DependencyService.Get<ISQLite>();
            _connection = iSqlite.GetConnection();
            IEnumerable<Restaurant> res = _connection.Query<Restaurant>("SELECT * FROM [Restaurant] where ranking <= 10");
            _connection.Close();
            return res;

        }
    }
}