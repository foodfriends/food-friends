﻿/*
 * Project Name: food Friends
 */

using System;
using SQLite;
using Xamarin.Forms;

namespace foodFriend
{
    public class Restaurant
    {
        [PrimaryKey, AutoIncrement]
        public int ID { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string Phone { get; set; }
        public string Img_Name { get; set; }
        public int Ranking { get; set; }
        public string Category { get; set; }
        public string Description { get; set; }
        public int Hour_start { get; set; }
        public int Hour_end { get; set; }
        public double Longtitude { get; set; }
        public double Latitude { get; set; }
        public int DBVersion { get; set; }
        public enum CategoryType
        {
            Asian = 0,
            Western = 1,
            Australian = 2
        }

        public void setCategory(CategoryType c)
        {
            switch (c)
            {
                case CategoryType.Asian: { Category = "Chinese"; break; }
                case CategoryType.Western: { Category = "Western"; break; }
                case CategoryType.Australian: { Category = "Pizza"; break; }
                default: { Category = "Unknown"; break; }

            }

        }
        public Restaurant()
        {
        }
    }
}

